-- MySQL Workbench Forward Engineering

SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0;
SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0;
SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='ONLY_FULL_GROUP_BY,STRICT_TRANS_TABLES,NO_ZERO_IN_DATE,NO_ZERO_DATE,ERROR_FOR_DIVISION_BY_ZERO,NO_ENGINE_SUBSTITUTION';

-- -----------------------------------------------------
-- Schema pizzaria
-- -----------------------------------------------------
DROP SCHEMA IF EXISTS `pizzaria` ;

-- -----------------------------------------------------
-- Schema pizzaria
-- -----------------------------------------------------
CREATE SCHEMA IF NOT EXISTS `pizzaria` DEFAULT CHARACTER SET utf8 ;
USE `pizzaria` ;

-- -----------------------------------------------------
-- Table `pizzaria`.`pizzas`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `pizzaria`.`pizzas` ;

CREATE TABLE IF NOT EXISTS `pizzaria`.`pizzas` (
  `id` INT NOT NULL AUTO_INCREMENT,
  `nome` VARCHAR(45) NOT NULL,
  `ingredientes` VARCHAR(255) NULL,
  `preco_pequena` DECIMAL(4,2) NOT NULL,
  `preco_media` DECIMAL(4,2) NOT NULL,
  `preco_grande` DECIMAL(4,2) NOT NULL,
  PRIMARY KEY (`id`))
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `pizzaria`.`funcionarios`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `pizzaria`.`funcionarios` ;

CREATE TABLE IF NOT EXISTS `pizzaria`.`funcionarios` (
  `id` INT NOT NULL AUTO_INCREMENT,
  `nome` VARCHAR(45) NOT NULL,
  `telefone` VARCHAR(45) NOT NULL,
  PRIMARY KEY (`id`))
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `pizzaria`.`clientes`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `pizzaria`.`clientes` ;

CREATE TABLE IF NOT EXISTS `pizzaria`.`clientes` (
  `id` INT NOT NULL AUTO_INCREMENT,
  `nome` VARCHAR(45) NOT NULL,
  `telefone1` VARCHAR(45) NOT NULL,
  `telefone2` VARCHAR(45) NULL,
  PRIMARY KEY (`id`))
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `pizzaria`.`formas_pagamentos`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `pizzaria`.`formas_pagamentos` ;

CREATE TABLE IF NOT EXISTS `pizzaria`.`formas_pagamentos` (
  `id` INT NOT NULL,
  `nome` VARCHAR(45) NOT NULL,
  PRIMARY KEY (`id`))
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `pizzaria`.`bairros_entregas`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `pizzaria`.`bairros_entregas` ;

CREATE TABLE IF NOT EXISTS `pizzaria`.`bairros_entregas` (
  `id` INT NOT NULL AUTO_INCREMENT,
  `nome` VARCHAR(45) NOT NULL,
  `tempo_entrega` INT NOT NULL,
  `valor_entrega` DECIMAL(4,2) NOT NULL DEFAULT 4,
  PRIMARY KEY (`id`))
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `pizzaria`.`enderecos_clientes`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `pizzaria`.`enderecos_clientes` ;

CREATE TABLE IF NOT EXISTS `pizzaria`.`enderecos_clientes` (
  `id` INT NOT NULL AUTO_INCREMENT,
  `logradouro` VARCHAR(120) NOT NULL,
  `cep` VARCHAR(8) NOT NULL,
  `numero` VARCHAR(6) NOT NULL,
  `complemento` VARCHAR(45) NULL,
  `municipio` VARCHAR(120) NOT NULL,
  `uf` VARCHAR(2) NOT NULL,
  `bairro_entrega_id` INT NOT NULL,
  `descricao` VARCHAR(45) NULL,
  `principal` TINYINT NOT NULL DEFAULT 0 COMMENT '0 - Endereço não padrão;\n1 - Endereço principal.',
  `cliente_id` INT NOT NULL,
  `ponto_referencia` VARCHAR(255) NULL,
  PRIMARY KEY (`id`),
  INDEX `fk_enderecos_clientes_bairros_entregas1_idx` (`bairro_entrega_id` ASC) VISIBLE,
  INDEX `fk_enderecos_clientes_clientes1_idx` (`cliente_id` ASC) VISIBLE,
  CONSTRAINT `fk_enderecos_clientes_bairros_entregas1`
    FOREIGN KEY (`bairro_entrega_id`)
    REFERENCES `pizzaria`.`bairros_entregas` (`id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `fk_enderecos_clientes_clientes1`
    FOREIGN KEY (`cliente_id`)
    REFERENCES `pizzaria`.`clientes` (`id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `pizzaria`.`situacoes_pedidos`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `pizzaria`.`situacoes_pedidos` ;

CREATE TABLE IF NOT EXISTS `pizzaria`.`situacoes_pedidos` (
  `id` INT NOT NULL,
  `nome` VARCHAR(45) NOT NULL,
  `ordem` TINYINT NOT NULL DEFAULT 0,
  `ativo` TINYINT NOT NULL DEFAULT 1,
  PRIMARY KEY (`id`))
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `pizzaria`.`pedidos`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `pizzaria`.`pedidos` ;

CREATE TABLE IF NOT EXISTS `pizzaria`.`pedidos` (
  `id` INT NOT NULL AUTO_INCREMENT,
  `data_hora_emissao` DATETIME NOT NULL,
  `data_hora_entrega` DATETIME NULL,
  `observacoes` VARCHAR(45) NULL,
  `funcionario_id` INT NOT NULL,
  `cliente_id` INT NOT NULL,
  `forma_pagamento_id` INT NOT NULL,
  `data_previsao_entrega` DATETIME NULL,
  `bairro_entrega_id` INT NOT NULL,
  `endereco_cliente_id` INT NOT NULL,
  `situacao_pedido_id` INT NOT NULL,
  `motivo_devolucao` VARCHAR(120) NULL,
  PRIMARY KEY (`id`),
  INDEX `fk_pedidos_funcionarios_idx` (`funcionario_id` ASC) VISIBLE,
  INDEX `fk_pedidos_cliente1_idx` (`cliente_id` ASC) VISIBLE,
  INDEX `fk_pedidos_formas_pagamentos1_idx` (`forma_pagamento_id` ASC) VISIBLE,
  INDEX `fk_pedidos_bairros_entregas1_idx` (`bairro_entrega_id` ASC) VISIBLE,
  INDEX `fk_pedidos_enderecos_clientes1_idx` (`endereco_cliente_id` ASC) VISIBLE,
  INDEX `fk_pedidos_situacoes_pedidos1_idx` (`situacao_pedido_id` ASC) VISIBLE,
  CONSTRAINT `fk_pedidos_funcionarios`
    FOREIGN KEY (`funcionario_id`)
    REFERENCES `pizzaria`.`funcionarios` (`id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `fk_pedidos_cliente1`
    FOREIGN KEY (`cliente_id`)
    REFERENCES `pizzaria`.`clientes` (`id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `fk_pedidos_formas_pagamentos1`
    FOREIGN KEY (`forma_pagamento_id`)
    REFERENCES `pizzaria`.`formas_pagamentos` (`id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `fk_pedidos_bairros_entregas1`
    FOREIGN KEY (`bairro_entrega_id`)
    REFERENCES `pizzaria`.`bairros_entregas` (`id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `fk_pedidos_enderecos_clientes1`
    FOREIGN KEY (`endereco_cliente_id`)
    REFERENCES `pizzaria`.`enderecos_clientes` (`id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `fk_pedidos_situacoes_pedidos1`
    FOREIGN KEY (`situacao_pedido_id`)
    REFERENCES `pizzaria`.`situacoes_pedidos` (`id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `pizzaria`.`itens_pizzas`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `pizzaria`.`itens_pizzas` ;

CREATE TABLE IF NOT EXISTS `pizzaria`.`itens_pizzas` (
  `id` INT NOT NULL AUTO_INCREMENT,
  `quantidade` INT NOT NULL,
  `tamanho` CHAR(1) NOT NULL COMMENT 'P, M ou G.',
  `pedido_id` INT NOT NULL,
  `pizza_id` INT NOT NULL,
  `observacao` VARCHAR(255) NULL,
  PRIMARY KEY (`id`),
  INDEX `fk_itens_pizzas_pedidos1_idx` (`pedido_id` ASC) VISIBLE,
  INDEX `fk_itens_pizzas_pizzas1_idx` (`pizza_id` ASC) VISIBLE,
  CONSTRAINT `fk_itens_pizzas_pedidos1`
    FOREIGN KEY (`pedido_id`)
    REFERENCES `pizzaria`.`pedidos` (`id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `fk_itens_pizzas_pizzas1`
    FOREIGN KEY (`pizza_id`)
    REFERENCES `pizzaria`.`pizzas` (`id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `pizzaria`.`ingredientes_extras`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `pizzaria`.`ingredientes_extras` ;

CREATE TABLE IF NOT EXISTS `pizzaria`.`ingredientes_extras` (
  `id` INT NOT NULL AUTO_INCREMENT,
  `nome` VARCHAR(45) NOT NULL,
  `preco_pequena` DECIMAL(4,2) NOT NULL DEFAULT 0,
  `preco_media` DECIMAL(4,2) NOT NULL DEFAULT 0,
  `preco_grande` DECIMAL(4,2) NOT NULL DEFAULT 0,
  PRIMARY KEY (`id`))
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `pizzaria`.`itens_ingredientes_extras`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `pizzaria`.`itens_ingredientes_extras` ;

CREATE TABLE IF NOT EXISTS `pizzaria`.`itens_ingredientes_extras` (
  `id` INT NOT NULL AUTO_INCREMENT,
  `quantidade` INT NOT NULL,
  `ingrediente_extra_id` INT NOT NULL,
  `item_pizzas_id` INT NOT NULL,
  PRIMARY KEY (`id`),
  INDEX `fk_itens_ingredientes_extras_ingredientes_extras1_idx` (`ingrediente_extra_id` ASC) VISIBLE,
  INDEX `fk_itens_ingredientes_extras_itens_pizzas1_idx` (`item_pizzas_id` ASC) VISIBLE,
  CONSTRAINT `fk_itens_ingredientes_extras_ingredientes_extras1`
    FOREIGN KEY (`ingrediente_extra_id`)
    REFERENCES `pizzaria`.`ingredientes_extras` (`id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `fk_itens_ingredientes_extras_itens_pizzas1`
    FOREIGN KEY (`item_pizzas_id`)
    REFERENCES `pizzaria`.`itens_pizzas` (`id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB;


SET SQL_MODE=@OLD_SQL_MODE;
SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS;
SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS;

-- -----------------------------------------------------
-- Data for table `pizzaria`.`formas_pagamentos`
-- -----------------------------------------------------
START TRANSACTION;
USE `pizzaria`;
INSERT INTO `pizzaria`.`formas_pagamentos` (`id`, `nome`) VALUES (1, 'A vista (Dinheiro)');
INSERT INTO `pizzaria`.`formas_pagamentos` (`id`, `nome`) VALUES (2, 'Débito');
INSERT INTO `pizzaria`.`formas_pagamentos` (`id`, `nome`) VALUES (3, 'Crédito');

COMMIT;


-- -----------------------------------------------------
-- Data for table `pizzaria`.`bairros_entregas`
-- -----------------------------------------------------
START TRANSACTION;
USE `pizzaria`;
INSERT INTO `pizzaria`.`bairros_entregas` (`id`, `nome`, `tempo_entrega`, `valor_entrega`) VALUES (DEFAULT, 'Treze de Maio', 60, DEFAULT);
INSERT INTO `pizzaria`.`bairros_entregas` (`id`, `nome`, `tempo_entrega`, `valor_entrega`) VALUES (DEFAULT, 'Mandacarú', 40, DEFAULT);
INSERT INTO `pizzaria`.`bairros_entregas` (`id`, `nome`, `tempo_entrega`, `valor_entrega`) VALUES (DEFAULT, 'Alto do Céu', 40, DEFAULT);
INSERT INTO `pizzaria`.`bairros_entregas` (`id`, `nome`, `tempo_entrega`, `valor_entrega`) VALUES (DEFAULT, 'Ipês', 60, DEFAULT);
INSERT INTO `pizzaria`.`bairros_entregas` (`id`, `nome`, `tempo_entrega`, `valor_entrega`) VALUES (DEFAULT, 'Estados', 60, DEFAULT);

COMMIT;


-- -----------------------------------------------------
-- Data for table `pizzaria`.`situacoes_pedidos`
-- -----------------------------------------------------
START TRANSACTION;
USE `pizzaria`;
INSERT INTO `pizzaria`.`situacoes_pedidos` (`id`, `nome`, `ordem`, `ativo`) VALUES (1, 'Aberto', 1, 1);
INSERT INTO `pizzaria`.`situacoes_pedidos` (`id`, `nome`, `ordem`, `ativo`) VALUES (2, 'Em preparação', 2, 1);
INSERT INTO `pizzaria`.`situacoes_pedidos` (`id`, `nome`, `ordem`, `ativo`) VALUES (3, 'Saiu para entrega', 3, 1);
INSERT INTO `pizzaria`.`situacoes_pedidos` (`id`, `nome`, `ordem`, `ativo`) VALUES (4, 'Retorno da entrega', 4, 1);
INSERT INTO `pizzaria`.`situacoes_pedidos` (`id`, `nome`, `ordem`, `ativo`) VALUES (5, 'Cancelado', 5, 1);
INSERT INTO `pizzaria`.`situacoes_pedidos` (`id`, `nome`, `ordem`, `ativo`) VALUES (6, 'Entregue', 6, 1);

COMMIT;


-- -----------------------------------------------------
-- Data for table `pizzaria`.`ingredientes_extras`
-- -----------------------------------------------------
START TRANSACTION;
USE `pizzaria`;
INSERT INTO `pizzaria`.`ingredientes_extras` (`id`, `nome`, `preco_pequena`, `preco_media`, `preco_grande`) VALUES (DEFAULT, 'Bacon', 1.5, 2.0, 2.5);
INSERT INTO `pizzaria`.`ingredientes_extras` (`id`, `nome`, `preco_pequena`, `preco_media`, `preco_grande`) VALUES (DEFAULT, 'Peperonni', 2.0, 2.5, 3.0);
INSERT INTO `pizzaria`.`ingredientes_extras` (`id`, `nome`, `preco_pequena`, `preco_media`, `preco_grande`) VALUES (DEFAULT, 'Ovo', 1.5, 2.0, 2.5);
INSERT INTO `pizzaria`.`ingredientes_extras` (`id`, `nome`, `preco_pequena`, `preco_media`, `preco_grande`) VALUES (DEFAULT, 'Milho', 1.5, 2.0, 2.5);
INSERT INTO `pizzaria`.`ingredientes_extras` (`id`, `nome`, `preco_pequena`, `preco_media`, `preco_grande`) VALUES (DEFAULT, 'Catupiry', 1.5, 2.0, 2.5);

COMMIT;

