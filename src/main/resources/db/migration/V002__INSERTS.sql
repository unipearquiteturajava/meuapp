
-- senha = teste123
INSERT INTO usuarios(username,senha,ativo) VALUES ('carlos','$2y$12$ZVC4BWvdj21/NNOA6RWiGeCYwQCBvZIqvatPnpqwEOcT0m.Fq.N0.', true); 
INSERT INTO usuarios(username,senha,ativo) VALUES ('maria','$2y$12$ZVC4BWvdj21/NNOA6RWiGeCYwQCBvZIqvatPnpqwEOcT0m.Fq.N0.', true);

INSERT INTO usuarios_roles (nome, role) VALUES ('carlos', 'ROLE_USER');
INSERT INTO usuarios_roles (nome, role) VALUES ('carlos', 'ROLE_ADMIN');
INSERT INTO usuarios_roles (nome, role) VALUES ('maria', 'ROLE_USER');

-- -----------------------------------------------------
-- Data for table `pizzaria`.`formas_pagamentos`
-- -----------------------------------------------------
INSERT INTO `pizzaria`.`formas_pagamentos` (`id`, `nome`) VALUES (1, 'A vista (Dinheiro)');
INSERT INTO `pizzaria`.`formas_pagamentos` (`id`, `nome`) VALUES (2, 'Débito');
INSERT INTO `pizzaria`.`formas_pagamentos` (`id`, `nome`) VALUES (3, 'Crédito');


-- -----------------------------------------------------
-- Data for table `pizzaria`.`bairros_entregas`
-- -----------------------------------------------------
INSERT INTO `pizzaria`.`bairros_entregas` (`id`, `nome`, `tempo_entrega`) VALUES (DEFAULT, 'Treze de Maio', 60);
INSERT INTO `pizzaria`.`bairros_entregas` (`id`, `nome`, `tempo_entrega`) VALUES (DEFAULT, 'Mandacarú', 40);
INSERT INTO `pizzaria`.`bairros_entregas` (`id`, `nome`, `tempo_entrega`) VALUES (DEFAULT, 'Alto do Céu', 40);
INSERT INTO `pizzaria`.`bairros_entregas` (`id`, `nome`, `tempo_entrega`) VALUES (DEFAULT, 'Ipês', 60);
INSERT INTO `pizzaria`.`bairros_entregas` (`id`, `nome`, `tempo_entrega`) VALUES (DEFAULT, 'Estados', 60);


-- -----------------------------------------------------
-- Data for table `pizzaria`.`situacoes_pedidos`
-- -----------------------------------------------------
INSERT INTO `pizzaria`.`situacoes_pedidos` (`id`, `nome`, `ordem`, `ativo`) VALUES (1, 'Aberto', 1, 1);
INSERT INTO `pizzaria`.`situacoes_pedidos` (`id`, `nome`, `ordem`, `ativo`) VALUES (2, 'Em preparação', 2, 1);
INSERT INTO `pizzaria`.`situacoes_pedidos` (`id`, `nome`, `ordem`, `ativo`) VALUES (3, 'Saiu para entrega', 3, 1);
INSERT INTO `pizzaria`.`situacoes_pedidos` (`id`, `nome`, `ordem`, `ativo`) VALUES (4, 'Retorno da entrega', 4, 1);
INSERT INTO `pizzaria`.`situacoes_pedidos` (`id`, `nome`, `ordem`, `ativo`) VALUES (5, 'Cancelado', 5, 1);
INSERT INTO `pizzaria`.`situacoes_pedidos` (`id`, `nome`, `ordem`, `ativo`) VALUES (6, 'Entregue', 6, 1);

-- -----------------------------------------------------
-- Data for table `pizzaria`.`ingredientes_extras`
-- -----------------------------------------------------
INSERT INTO `pizzaria`.`ingredientes_extras` (`id`, `nome`, `preco_pequena`, `preco_media`, `preco_grande`) VALUES (DEFAULT, 'Bacon', 1.5, 2.0, 2.5);
INSERT INTO `pizzaria`.`ingredientes_extras` (`id`, `nome`, `preco_pequena`, `preco_media`, `preco_grande`) VALUES (DEFAULT, 'Peperonni', 2.0, 2.5, 3.0);
INSERT INTO `pizzaria`.`ingredientes_extras` (`id`, `nome`, `preco_pequena`, `preco_media`, `preco_grande`) VALUES (DEFAULT, 'Ovo', 1.5, 2.0, 2.5);
INSERT INTO `pizzaria`.`ingredientes_extras` (`id`, `nome`, `preco_pequena`, `preco_media`, `preco_grande`) VALUES (DEFAULT, 'Milho', 1.5, 2.0, 2.5);
INSERT INTO `pizzaria`.`ingredientes_extras` (`id`, `nome`, `preco_pequena`, `preco_media`, `preco_grande`) VALUES (DEFAULT, 'Catupiry', 1.5, 2.0, 2.5);


