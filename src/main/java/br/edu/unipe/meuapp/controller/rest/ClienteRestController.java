package br.edu.unipe.meuapp.controller.rest;

import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import br.edu.unipe.meuapp.model.entity.Cliente;
import br.edu.unipe.meuapp.model.service.ClienteService;
import io.swagger.annotations.Api;
import lombok.extern.slf4j.Slf4j;

@Slf4j
@RestController
@RequestMapping(path = "/clientes", produces = {"application/json"})
@Api(value = "clientes")
public class ClienteRestController extends GenericCrudRestController<Cliente, Integer, ClienteService> {


}