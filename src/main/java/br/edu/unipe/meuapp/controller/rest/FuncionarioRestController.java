package br.edu.unipe.meuapp.controller.rest;

import java.util.List;

import javax.validation.Valid;

import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import br.edu.unipe.meuapp.exception.AplicacaoException;
import br.edu.unipe.meuapp.model.entity.Funcionario;
import br.edu.unipe.meuapp.model.service.FuncionarioService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;
import lombok.extern.slf4j.Slf4j;

@Slf4j
@RestController
@RequestMapping(path = "/funcionarios", produces = {"application/json"})
@Api(value = "funcionarios")
public class FuncionarioRestController extends GenericCrudRestController<Funcionario, Integer, FuncionarioService> {

	@Override
	@ApiOperation(value = "Cadastra um funcionario.", nickname = "salvar", notes = "", response = Funcionario.class)
	@ApiResponses(value = { @ApiResponse(code = 405, message = "Validation exception") })
	@RequestMapping(value = "/", method = RequestMethod.POST)
	public @ResponseBody ResponseEntity<Funcionario> salvar(
			@ApiParam(value = "Objeto pizza a ser cadastrada.", required = true) @Valid final @RequestBody Funcionario entity)
			throws AplicacaoException {

		return super.salvar(entity);
	}

	@ApiOperation(value = "Atualiza um funcionario existente.", nickname = "alterar", notes = "", response = Funcionario.class)
	@ApiResponses(value = { @ApiResponse(code = 400, message = "ID inválido"),
			@ApiResponse(code = 404, message = "Funcionario não encontrado"),
			@ApiResponse(code = 405, message = "Validation exception") })
	@RequestMapping(value = "/{id}", method = RequestMethod.PUT)
	public @ResponseBody ResponseEntity<Funcionario> alterar(
			@ApiParam(value = "Objeto entida a ser atualizada.", required = true) @Valid final @RequestBody Funcionario entity,
			final @PathVariable Integer id) throws AplicacaoException {

		return super.alterar(entity, id);
	}

	@Override
	@ApiOperation(value = "Remove um funcionario existente.", nickname = "alterar", notes = "", response = Funcionario.class)
	@ApiResponses(value = { @ApiResponse(code = 400, message = "ID inválido"),
			@ApiResponse(code = 404, message = "Funcionario não encontrado"),
			@ApiResponse(code = 405, message = "Validation exception") })
	@RequestMapping(value = "/{id}", method = RequestMethod.DELETE)
	public ResponseEntity<Void> remover(final @PathVariable Integer id) throws AplicacaoException {
		return super.remover(id);
	}

	@Override
	@ApiOperation(value = "Remove um funcionario existente.", nickname = "alterar", notes = "", response = Funcionario.class, responseContainer = "List")
	@ApiResponses(value = { 
			@ApiResponse(code = 204, message = "Nenhum funcionario não encontrado") })
	@RequestMapping(value = "/", method = RequestMethod.GET)
	public @ResponseBody ResponseEntity<List<Funcionario>> listar() throws AplicacaoException {
		return super.listar();
	}

	@Override
	@ApiOperation(value = "Remove um funcionario existente.", nickname = "alterar", notes = "", response = Funcionario.class, responseContainer = "List")
	@ApiResponses(value = { 
			@ApiResponse(code = 204, message = "Nenhum funcionario não encontrado") })
	@RequestMapping(value = "/{id}", method = RequestMethod.GET)
	public @ResponseBody ResponseEntity<Funcionario> buscar(final @PathVariable Integer id) throws AplicacaoException {
		return super.buscar(id);
	}

}