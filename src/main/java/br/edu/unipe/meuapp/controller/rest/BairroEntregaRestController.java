package br.edu.unipe.meuapp.controller.rest;

import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import br.edu.unipe.meuapp.model.entity.BairroEntrega;
import br.edu.unipe.meuapp.model.service.BairroEntregaService;
import io.swagger.annotations.Api;
import lombok.extern.slf4j.Slf4j;

@Slf4j
@RestController
@RequestMapping(path = "/bairros-entregas", produces = {"application/json"})
@Api(value = "bairros-entregas")
public class BairroEntregaRestController extends GenericCrudRestController<BairroEntrega, Integer, BairroEntregaService> {


}