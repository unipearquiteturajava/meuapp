package br.edu.unipe.meuapp.controller.rest;

import javax.validation.Valid;

import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import br.edu.unipe.meuapp.exception.AplicacaoException;
import br.edu.unipe.meuapp.model.dto.OperacaoPedido;
import br.edu.unipe.meuapp.model.entity.Pedido;
import br.edu.unipe.meuapp.model.service.PedidoService;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;
import lombok.extern.slf4j.Slf4j;

@Slf4j
@RestController
@RequestMapping("/pedidos")
public class PedidosRestController extends GenericCrudRestController<Pedido, Integer, PedidoService> {

	@ApiOperation(value = "Marca o pedido como entregue.", nickname = "entregar", notes = "")
	@ApiResponses(value = { @ApiResponse(code = 400, message = "ID inválido"),
			@ApiResponse(code = 404, message = "Entidade não encontrada"),
			@ApiResponse(code = 405, message = "Validation exception") })
	@RequestMapping(value = "/{id}/entregar", method = RequestMethod.PUT)
	public @ResponseBody ResponseEntity<Pedido> entregar(
			@ApiParam(value = "Objeto entida a ser atualizada.", required = true) @Valid final @RequestBody OperacaoPedido operacao,
			final @PathVariable Integer id) throws AplicacaoException {

		if (log.isDebugEnabled()) {

			log.debug("Realizando a chamada do controller: " + this.getClass().getName() + ".entregar( "
					+ operacao.getClass().getName() + " , " + id + " ). Realizando a chamada do service: "
					+ service.getClass().getName() + ".entregar( " + id + ", " + operacao.getClass().getName() + " )");
		}

		Pedido pedido = service.entregar(id, operacao);

		if (log.isDebugEnabled()) {

			log.debug("Chamada do controller: " + this.getClass().getName() + ".entregar( "
					+ operacao.getClass().getName() + " ) realizada com sucesso.");
		}

		return new ResponseEntity<Pedido>(pedido, HttpStatus.OK);
	}
	
	@ApiOperation(value = "Marca o pedido como devolvido (retornado da entrega).", nickname = "entregar", notes = "")
	@ApiResponses(value = { @ApiResponse(code = 400, message = "ID inválido"),
			@ApiResponse(code = 404, message = "Entidade não encontrada"),
			@ApiResponse(code = 405, message = "Validation exception") })
	@RequestMapping(value = "/{id}/devolver", method = RequestMethod.PUT)
	public @ResponseBody ResponseEntity<Pedido> devolver(
			@ApiParam(value = "Objeto entida a ser atualizada.", required = true) @Valid final @RequestBody OperacaoPedido operacao,
			final @PathVariable Integer id) throws AplicacaoException {

		if (log.isDebugEnabled()) {

			log.debug("Realizando a chamada do controller: " + this.getClass().getName() + ".devolver( "
					+ operacao.getClass().getName() + " , " + id + " ). Realizando a chamada do service: "
					+ service.getClass().getName() + ".devolver( " + id + ", " + operacao.getClass().getName() + " )");
		}

		Pedido pedido = service.devolver(id, operacao);

		if (log.isDebugEnabled()) {

			log.debug("Chamada do controller: " + this.getClass().getName() + ".devolver( "
					+ operacao.getClass().getName() + " ) realizada com sucesso.");
		}

		return new ResponseEntity<Pedido>(pedido, HttpStatus.OK);
	}

}