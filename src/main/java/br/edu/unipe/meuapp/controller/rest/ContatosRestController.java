package br.edu.unipe.meuapp.controller.rest;

import java.util.concurrent.Future;

import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import br.edu.unipe.meuapp.exception.AplicacaoException;
import br.edu.unipe.meuapp.model.entity.Contato;
import br.edu.unipe.meuapp.model.service.ContatoService;
import lombok.extern.slf4j.Slf4j;

@Slf4j
@RestController
@RequestMapping("/contatos-rest")
public class ContatosRestController extends GenericCrudRestController<Contato, Long, ContatoService> {

	@RequestMapping(value = "/async", method = RequestMethod.GET)
	public void async() throws AplicacaoException {
//		service.asyncMethodWithReturnType();
//		Future<String> future = service.asyncMethodWithReturnType();
//		while (!future.isDone()) {
//			log.info("LOAD()...");
//		} 
//		log.info("DONE");
	}

}