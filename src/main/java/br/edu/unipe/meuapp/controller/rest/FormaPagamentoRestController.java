package br.edu.unipe.meuapp.controller.rest;

import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import br.edu.unipe.meuapp.model.entity.FormaPagamento;
import br.edu.unipe.meuapp.model.service.FormaPagamentoService;
import io.swagger.annotations.Api;
import lombok.extern.slf4j.Slf4j;

@Slf4j
@RestController
@RequestMapping(path = "/formas-pagamentos", produces = {"application/json"})
@Api(value = "formas-pagamentos")
public class FormaPagamentoRestController extends GenericCrudRestController<FormaPagamento, Integer, FormaPagamentoService> {


}