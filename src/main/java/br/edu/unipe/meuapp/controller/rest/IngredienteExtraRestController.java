package br.edu.unipe.meuapp.controller.rest;

import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import br.edu.unipe.meuapp.model.entity.IngredienteExtra;
import br.edu.unipe.meuapp.model.service.IngredienteExtraService;
import io.swagger.annotations.Api;
import lombok.extern.slf4j.Slf4j;

@Slf4j
@RestController
@RequestMapping(path = "/ingredientes-extras", produces = {"application/json"})
@Api(value = "ingredientes-extras")
public class IngredienteExtraRestController extends GenericCrudRestController<IngredienteExtra, Integer, IngredienteExtraService> {


}