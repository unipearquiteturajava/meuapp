package br.edu.unipe.meuapp.controller.rest;

import java.util.List;

import javax.validation.Valid;

import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import br.edu.unipe.meuapp.exception.AplicacaoException;
import br.edu.unipe.meuapp.model.entity.Pizza;
import br.edu.unipe.meuapp.model.entity.SituacaoPedido;
import br.edu.unipe.meuapp.model.service.SituacaoPedidoService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;
import lombok.extern.slf4j.Slf4j;

@Slf4j
@RestController
@RequestMapping(path = "/situacoes-pedidos", produces = {"application/json"})
@Api(value = "situacoes-pedidos")
public class SituacaoPedidoRestController extends GenericCrudRestController<SituacaoPedido, Integer, SituacaoPedidoService> {


}