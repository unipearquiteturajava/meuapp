package br.edu.unipe.meuapp.controller.rest;

import java.util.List;

import javax.validation.Valid;

import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import br.edu.unipe.meuapp.exception.AplicacaoException;
import br.edu.unipe.meuapp.model.entity.Pizza;
import br.edu.unipe.meuapp.model.service.PizzaService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;
import lombok.extern.slf4j.Slf4j;

@Slf4j
@RestController
@RequestMapping(path = "/pizzas", produces = {"application/json"})
@Api(value = "pizzas")
public class PizzaRestController extends GenericCrudRestController<Pizza, Integer, PizzaService> {

	@Override
	@ApiOperation(value = "Cadastra uma pizza.", nickname = "salvar", notes = "", response = Pizza.class)
	@ApiResponses(value = { @ApiResponse(code = 405, message = "Validation exception") })
	@RequestMapping(value = "/", method = RequestMethod.POST)
	public @ResponseBody ResponseEntity<Pizza> salvar(
			@ApiParam(value = "Objeto pizza a ser cadastrada.", required = true) @Valid final @RequestBody Pizza entity)
			throws AplicacaoException {

		return super.salvar(entity);
	}

	@ApiOperation(value = "Atualiza uma pizza existente.", nickname = "alterar", notes = "", response = Pizza.class)
	@ApiResponses(value = { @ApiResponse(code = 400, message = "ID inválido"),
			@ApiResponse(code = 404, message = "pizza não encontrada"),
			@ApiResponse(code = 405, message = "Validation exception") })
	@RequestMapping(value = "/{id}", method = RequestMethod.PUT)
	public @ResponseBody ResponseEntity<Pizza> alterar(
			@ApiParam(value = "Objeto entida a ser atualizada.", required = true) @Valid final @RequestBody Pizza entity,
			final @PathVariable Integer id) throws AplicacaoException {

		return super.alterar(entity, id);
	}

	@Override
	@ApiOperation(value = "Remove uma pizza existente.", nickname = "alterar", notes = "", response = Pizza.class)
	@ApiResponses(value = { @ApiResponse(code = 400, message = "ID inválido"),
			@ApiResponse(code = 404, message = "pizza não encontrada"),
			@ApiResponse(code = 405, message = "Validation exception") })
	@RequestMapping(value = "/{id}", method = RequestMethod.DELETE)
	public ResponseEntity<Void> remover(final @PathVariable Integer id) throws AplicacaoException {
		return super.remover(id);
	}

	@Override
	@ApiOperation(value = "Remove uma pizza existente.", nickname = "alterar", notes = "", response = Pizza.class, responseContainer = "List")
	@ApiResponses(value = { 
			@ApiResponse(code = 204, message = "Nenhuma pizza não encontrada") })
	@RequestMapping(value = "/", method = RequestMethod.GET)
	public @ResponseBody ResponseEntity<List<Pizza>> listar() throws AplicacaoException {
		return super.listar();
	}

	@Override
	@ApiOperation(value = "Remove uma pizza existente.", nickname = "alterar", notes = "", response = Pizza.class, responseContainer = "List")
	@ApiResponses(value = { 
			@ApiResponse(code = 204, message = "Nenhuma pizza não encontrada") })
	@RequestMapping(value = "/{id}", method = RequestMethod.GET)
	public @ResponseBody ResponseEntity<Pizza> buscar(final @PathVariable Integer id) throws AplicacaoException {
		return super.buscar(id);
	}

}