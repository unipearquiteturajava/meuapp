package br.edu.unipe.meuapp.model.entity;

import java.util.LinkedList;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.validation.constraints.NotEmpty;

import org.springframework.util.CollectionUtils;

import com.fasterxml.jackson.annotation.JsonIgnore;

import lombok.Data;
import lombok.NoArgsConstructor;

@NoArgsConstructor
@Data
@Entity
@Table(name="clientes")
public class Cliente extends GenericEntity<Integer>{
	
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Integer id;
	
	@NotEmpty(message="Nome é obrigatório")
	private String nome;
	
	@NotEmpty(message="Telefone1 é obrigatório")
	private String telefone1;
	
	private String telefone2;
	
	@JsonIgnore
	@OneToMany(mappedBy="cliente", cascade = {CascadeType.ALL})
	private List<EnderecoCliente> enderecos;
	
	public void addEnderecoCliente(EnderecoCliente endereco) {
		if(CollectionUtils.isEmpty(this.enderecos)) {
			this.enderecos = new LinkedList<>();
		}
		endereco.setCliente(this);
		this.enderecos.add(endereco);
	}

}
