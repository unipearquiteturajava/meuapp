package br.edu.unipe.meuapp.model.service;

import org.springframework.stereotype.Service;

import br.edu.unipe.meuapp.model.entity.Pizza;
import br.edu.unipe.meuapp.model.repository.PizzaRepository;
import lombok.extern.slf4j.Slf4j;

@Slf4j
@Service
public class PizzaService extends GenericCrudService<Pizza, Integer, PizzaRepository>{

//	private static int COUNT_INSERTS = 0;
	
//	@Scheduled(fixedDelay=6000)
//	public void asyncMethodWithReturnType() {
//	    log.info("Execute method asynchronously - "
//	      + Thread.currentThread().getName());
//    	
//	    COUNT_INSERTS++;
//    	Contato contato = new Contato();
//    	contato.setNome("Contato" + COUNT_INSERTS);
//    	contato.setEmail("contato" + COUNT_INSERTS + "@email.com");
//    	contato.setAtivo(COUNT_INSERTS % 2 == 0 ? true : false);
//    	
//    	log.info("Salvando contato: " + contato.getNome());
//    	salvar(contato);
//    	
//    	List<Contato> contatos = listar();
//    	log.info("Listando contatos: " + contatos.size());
//    	for (Contato contato2 : contatos) {
//			log.info("Contato: " + contato2.getNome() + " - " + contato2.getEmail() + "[" + (contato2.getAtivo().booleanValue() ? "Ativo" : "Inativo")  + "]");
//		}
//	        
//	    
//	}
	
}
