package br.edu.unipe.meuapp.model.dto;

import java.time.LocalDateTime;

import lombok.Data;
import lombok.Getter;
import lombok.Setter;

@Data
@Getter
@Setter
public class OperacaoPedido {
	
	private LocalDateTime momento;
	
	private String motivo;
	
	
	public OperacaoPedido() {
		// Construtor padrão
	}
	
	public OperacaoPedido(String motivo, LocalDateTime momento) {
		this.motivo = motivo;
		this.momento = momento;
	}
	
	public OperacaoPedido(LocalDateTime momento) {
		this(null, momento);
	}
	
	public OperacaoPedido(String motivo) {
		this(motivo, LocalDateTime.now());
	}

}
