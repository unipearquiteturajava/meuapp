package br.edu.unipe.meuapp.model.entity;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.persistence.Transient;
import javax.validation.constraints.Min;
import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;

import com.fasterxml.jackson.annotation.JsonIgnore;

import lombok.Data;
import lombok.NoArgsConstructor;

@NoArgsConstructor
@Data
@Entity
@Table(name="itens_ingredientes_extras")
public class ItemIngredienteExtra extends GenericEntity<Integer>{
	
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Integer id;
	
	@NotNull(message="Quantidade é obrigatória")
	@Min(value=1)	
	private Integer quantidade;
	
	@ManyToOne
	@JoinColumn(name="ingrediente_extra_id")
	private IngredienteExtra ingredienteExtra;
	
	@JsonIgnore
	@ManyToOne
	@JoinColumn(name="item_pizzas_id")
	private ItemPedidoPizza itemPedidoPizza;
	
}
