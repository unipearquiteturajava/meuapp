package br.edu.unipe.meuapp.model.entity;

import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.validation.constraints.NotEmpty;

import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@Entity
@Table(name="situacoes_pedidos")
public class SituacaoPedido extends GenericEntity<Integer>{
	
	public static final int ID_ABERTO = 1;
	public static final int ID_EM_PREPARACAO = 2;
	public static final int ID_SAIU_ENTREGA = 3;
	public static final int ID_RETORNO_ENTREGA = 4;
	public static final int ID_CANCELADO = 5;
	public static final int ID_ENTREGUE = 6;
	
	private static final long serialVersionUID = 1L;
	
	@Id
	private Integer id;
	
	@NotEmpty(message="Nome é obrigatório")
	private String nome;
	
	private Integer ordem;
	
	@NotEmpty(message="Ativo é obrigatório")
	private Boolean ativo;
	
	public SituacaoPedido() {
		super();
	}
	
	public SituacaoPedido(Integer id) {
		super();
		setId(id);
	}

}
