package br.edu.unipe.meuapp.model.entity;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;

import lombok.Data;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@NoArgsConstructor
@Data
@Getter
@Setter
@Entity
@Table(name="bairros_entregas")
public class BairroEntrega extends GenericEntity<Integer>{
	
	private static final long serialVersionUID = 1L;

	@Id
	private Integer id;
	
	@NotEmpty(message="Nome é obrigatório")
	private String nome;
	
	@NotNull(message="Tempo de entrega é obrigatório")
	@Column(name="tempo_entrega")
	private Integer tempoEntrega;
	
	@NotNull(message="Valor de entrega é obrigatório")
	@Column(name="valor_entrega")
	private Float valorEntrega;
	
}
