package br.edu.unipe.meuapp.model.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.repository.NoRepositoryBean;

import br.edu.unipe.meuapp.model.entity.GenericEntity;



/**
 * @author eder
 *
 * @param <T>
 * @param <ID>
 */
@NoRepositoryBean
public interface GenericCrudRepository<T extends GenericEntity<I>, I> extends JpaRepository<T, I>{



	
}
