package br.edu.unipe.meuapp.model.entity;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.Transient;
import javax.validation.constraints.NotEmpty;

import lombok.Data;
import lombok.NoArgsConstructor;

@NoArgsConstructor
@Data
@Entity
@Table(name="ingredientes_extras")
public class IngredienteExtra extends GenericEntity<Integer>{
	
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Integer id;
	
	@NotEmpty(message="Nome é obrigatório")
	private String nome;
	
	@NotEmpty(message="O preço de pizzas pequenas é obrigatório")
	@Column(name="preco_pequena")
	private Float precoPequena;
	
	@NotEmpty(message="O preço de pizzas médias é obrigatório")
	@Column(name="preco_media")
	private Float precoMedia;
	
	@NotEmpty(message="O preço de pizzas grandes é obrigatório")
	@Column(name="preco_grande")
	private Float precoGrande;
	
	@Transient
	public Float getPrecoTamanho(TamanhoPizza tamanho) {
		switch (tamanho) {
		case P:
			return getPrecoPequena();
		case M:
			return getPrecoMedia();
		case G:
			return getPrecoGrande();
		default:
			throw new RuntimeException("Não foi encontrado valor para o tamanho cidato.");
		}
	}

}
