package br.edu.unipe.meuapp.model.service;

import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Service;

import br.edu.unipe.meuapp.model.entity.Contato;
import br.edu.unipe.meuapp.model.repository.ContatosRepository;
import lombok.extern.slf4j.Slf4j;

@Slf4j
@Service
public class ContatoService extends GenericCrudService<Contato, Long, ContatosRepository>{

//	@Scheduled(fixedDelay=6000)
	public void salvarContato() {
		
		
		Contato c = new Contato();
		c.setEmail("fmeder@gmail.com");
		c.setNome("EDER");
		this.salvar(c);
		
		for (Contato co : this.listar()) {
			log.info(co.getNome());
		}
	}
	
}
