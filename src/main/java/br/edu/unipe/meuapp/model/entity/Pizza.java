package br.edu.unipe.meuapp.model.entity;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.Transient;
import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

import lombok.Data;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@NoArgsConstructor
@Data
@Getter
@Setter
@JsonIgnoreProperties(ignoreUnknown=true)
@Entity
@Table(name="pizzas")
public class Pizza extends GenericEntity<Integer>{
	
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Integer id;
	
	@NotEmpty(message="Nome é obrigatório")
	private String nome;
	
	private String ingredientes;
	
	@NotNull(message="O preço de pizzas pequenas é obrigatório")
	@Column(name="preco_pequena")
	private Float precoPequena;
	
	@NotNull(message="O preço de pizzas médias é obrigatório")
	@Column(name="preco_media")
	private Float precoMedia;
	
	@NotNull(message="O preço de pizzas grandes é obrigatório")
	@Column(name="preco_grande")
	private Float precoGrande;
	
	@Transient
	public Float getPrecoTamanho(TamanhoPizza tamanho) {
		switch (tamanho) {
		case P:
			return getPrecoPequena();
		case M:
			return getPrecoMedia();
		case G:
			return getPrecoGrande();
		default:
			throw new RuntimeException("Não foi encontrado valor para o tamanho cidato.");
		}
	}

}
