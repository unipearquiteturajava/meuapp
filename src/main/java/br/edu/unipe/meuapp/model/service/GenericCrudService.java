package br.edu.unipe.meuapp.model.service;

import java.lang.reflect.Field;
import java.util.LinkedList;
import java.util.List;
import java.util.Objects;
import java.util.Optional;

import javax.validation.ConstraintViolation;

import org.apache.commons.lang3.StringUtils;
import org.glassfish.jersey.internal.guava.Lists;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cache.annotation.Cacheable;
import org.springframework.transaction.annotation.Transactional;

import br.edu.unipe.meuapp.exception.AplicacaoException;
import br.edu.unipe.meuapp.exception.AplicacaoExceptionValue;
import br.edu.unipe.meuapp.exception.MeuappValidacoes;
import br.edu.unipe.meuapp.exception.ValidacaoCampos;
import br.edu.unipe.meuapp.model.entity.GenericEntity;
import br.edu.unipe.meuapp.model.repository.GenericCrudRepository;
import lombok.extern.slf4j.Slf4j;


/**
 * @author eder
 *
 * @param <T>
 */
@Slf4j
public abstract class GenericCrudService<T extends GenericEntity<I>, I, R extends GenericCrudRepository<T, I>> {

	
	@Autowired
	protected R repository;

	@Transactional(rollbackFor = Throwable.class)
	public T salvar(T entidade) throws AplicacaoException{
		entidade.setPersisted(existe(entidade.getId()));
		ajustar(entidade);
		validar(entidade);
		entidade = repository.save(entidade);
		return entidade;

	}
	
	public Long contar() {
		return repository.count();
	}

//	@Cacheable(value="listarCache")
	public List<T> listar() {
		return Lists.newArrayList(repository.findAll());
	}

//	@Cacheable(value="buscarCache", key="{ #id }")
	public T buscar(I id) throws AplicacaoException {
		Optional<T> retorno = repository.findById(id);
		if(retorno != null && retorno.isPresent()) {
			return retorno.get();
		}
		return null;
	}

	public boolean existe(I id) {
		return !Objects.isNull(id) && repository.existsById(id);
	}

	@Transactional(rollbackFor = Throwable.class)
	public void remover(T entidade) throws AplicacaoException {
		try {
			repository.delete(entidade);
		} catch (Exception e) {
			throw new AplicacaoException(MeuappValidacoes.ERRO_EXCLUSAO_GENERICO, e);
		}
	}

	@Transactional(rollbackFor = Throwable.class)
	public void remover(I id) throws AplicacaoException {
		try {
			repository.deleteById(id);
		} catch (Exception e) {
			throw new AplicacaoException(MeuappValidacoes.ERRO_EXCLUSAO_GENERICO, e);
		}
	}

	@Transactional(rollbackFor = Throwable.class)
	public void removerTodos() {
		repository.deleteAll();
	}

	private void ajustar(T entidade) {
		if (entidade == null) {
			return;
		}

		try {
			Field[] campos = entidade.getClass().getDeclaredFields();

			for (Field f : campos) {
				f.setAccessible(true);
				Object object = f.get(entidade);
				if (object instanceof CharSequence) {
					object.toString().trim();
				}
			}

		} catch (Exception e) {
			log.error(e.getMessage(), e);
		}
	}

	
	private void validar(T entidade) {
		List<AplicacaoExceptionValue> customExceptionValues = new LinkedList<>();

		for (ConstraintViolation<GenericEntity<I>> error : entidade.validationsConstraintsFails()) {

			AplicacaoExceptionValue customExceptionValue = new AplicacaoExceptionValue(
					ValidacaoCampos.newInstance(StringUtils.substringBetween(error.getMessageTemplate(), "{", "}"),
							error.getMessage()),
					true, error.getPropertyPath().toString(),
					error.getInvalidValue() != null ? error.getInvalidValue().toString() : null);

			customExceptionValues.add(customExceptionValue);
		}

		if (!customExceptionValues.isEmpty()) {
			throw new AplicacaoException(MeuappValidacoes.ERRO_VALIDACAO, customExceptionValues);
		}

	}

}