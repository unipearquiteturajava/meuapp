package br.edu.unipe.meuapp.model.entity;

import java.time.LocalDateTime;
import java.util.List;
import java.util.stream.Collectors;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.persistence.Transient;
import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;

import org.springframework.format.annotation.DateTimeFormat;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.annotation.JsonFormat.Shape;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.fasterxml.jackson.datatype.jsr310.ser.LocalDateTimeSerializer;

import lombok.Data;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@NoArgsConstructor
@Data
@Getter
@Setter
@Entity
@Table(name="pedidos")
public class Pedido extends GenericEntity<Integer>{
	
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Integer id;
	
	@JsonProperty(required = false)
	@Column(name = "data_hora_emissao")
	private LocalDateTime dataHoraEmissao;
	
	@Column(name = "data_hora_entrega")
	private LocalDateTime dataHoraEntrega;
	
	@Column(name = "data_previsao_entrega")
	private LocalDateTime dataPrevisaoEntrega;
	
	@JsonProperty(required = true)
	@ManyToOne
	@JoinColumn(name="funcionario_id")
	private Funcionario funcionario;
	
	@JsonProperty(required = true)
	@ManyToOne(cascade={CascadeType.PERSIST, CascadeType.MERGE, CascadeType.REFRESH})
	@JoinColumn(name="cliente_id")
	private Cliente cliente;
	
	@ManyToOne
	@JoinColumn(name="forma_pagamento_id")
	private FormaPagamento formaPagamento;
	
	@JsonProperty(required = true)
	@ManyToOne
	@JoinColumn(name="bairro_entrega_id")
	private BairroEntrega bairroEntrega;
	
	@ManyToOne(cascade={CascadeType.PERSIST, CascadeType.MERGE, CascadeType.REFRESH})
	@JoinColumn(name="endereco_cliente_id")
	private EnderecoCliente enderecoEntrega;
	
	@ManyToOne
	@JoinColumn(name="situacao_pedido_id")
	private SituacaoPedido situacao;
	
	@OneToMany(mappedBy="pedido", cascade = CascadeType.ALL , fetch = FetchType.EAGER)
	private List<ItemPedidoPizza> itensPizzas;
	
	private String observacoes;
	
	@Column(name="motivo_devolucao")
	private String motivoDevolucao;
	
	@JsonProperty(value="subTotalPedido")
	@Transient
	public Float getSubTotalPedido() {
		return itensPizzas.stream().map(item -> item.getTotalItem()).collect(Collectors.toList()).stream().reduce(0.0f, (a, b) -> a + b);
	}
	
	@JsonProperty(value="totalPedido")
	@Transient
	public Float getTotalPedido() {
		return getSubTotalPedido() + bairroEntrega.getValorEntrega();
	}

}
