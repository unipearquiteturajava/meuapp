package br.edu.unipe.meuapp.model.entity;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.validation.constraints.NotEmpty;

import lombok.Data;
import lombok.NoArgsConstructor;

@NoArgsConstructor
@Data
@Entity
@Table(name="formas_pagamentos")
public class FormaPagamento extends GenericEntity<Integer>{
	
	private static final long serialVersionUID = 1L;

	@Id
	private Integer id;
	
	@NotEmpty(message="Nome é obrigatório")
	private String nome;
	
}
