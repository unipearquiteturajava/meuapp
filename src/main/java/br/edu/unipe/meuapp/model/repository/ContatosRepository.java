package br.edu.unipe.meuapp.model.repository;

import java.util.List;

import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import br.edu.unipe.meuapp.model.entity.Contato;

public interface ContatosRepository extends GenericCrudRepository<Contato, Long> {

	
	@Query(value="select c.nome from TB_CONTATO c where c.id = :id", nativeQuery=true)
	public String buscarPorNomeSQLNativo(@Param("id") Long id);
	
	@Query("select c from Contato c where c.nome like :nome")
	public List<Contato> buscarPorNome(@Param("nome") String nome);
	
	public List<Contato> findByNomeContaining(String nome);
	
	@Modifying
	@Query("update Contato c set c.nome=:nome where c.email=:email")
	public void atualizaNomePeloEmail(@Param("nome") String nome, @Param("email") String email);
}
