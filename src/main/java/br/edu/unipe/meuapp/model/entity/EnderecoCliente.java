package br.edu.unipe.meuapp.model.entity;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;

import com.fasterxml.jackson.annotation.JsonIgnore;

import lombok.Data;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@NoArgsConstructor
@Data
@Getter
@Setter
@Entity
@Table(name="enderecos_clientes")
public class EnderecoCliente extends GenericEntity<Integer>{
	
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Integer id;
	
	private String descricao;
	
	@NotEmpty(message="CEP é obrigatório")
	private String cep;
	
	@NotEmpty(message="Logradouro é obrigatório")
	private String logradouro;
	
	@NotEmpty(message="Número é obrigatório")
	private String numero;
	
	private String complemento;
	
	@NotEmpty(message="Município é obrigatório")
	private String municipio;
	
	@NotEmpty(message="UF é obrigatória")
	private String uf;
	
	@NotNull(message="Principal é obrigatória")
	private Boolean principal;
	
	@Column(name="ponto_referencia")
	private String pontoReferencia;
	
	@ManyToOne
	@JoinColumn(name="bairro_entrega_id")
	private BairroEntrega bairroEntrega;
	
	@JsonIgnore
	@ManyToOne
	@JoinColumn(name="cliente_id")
	private Cliente cliente;

}
