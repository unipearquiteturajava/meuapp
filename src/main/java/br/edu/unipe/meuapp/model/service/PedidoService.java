package br.edu.unipe.meuapp.model.service;

import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.temporal.Temporal;
import java.time.temporal.TemporalUnit;
import java.util.LinkedList;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

import javax.transaction.Transactional;
import javax.transaction.Transactional.TxType;

import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Service;
import org.springframework.util.CollectionUtils;

import com.fasterxml.jackson.datatype.threetenbp.deser.LocalDateTimeDeserializer;

import br.edu.unipe.meuapp.exception.AplicacaoException;
import br.edu.unipe.meuapp.model.dto.OperacaoPedido;
import br.edu.unipe.meuapp.model.entity.BairroEntrega;
import br.edu.unipe.meuapp.model.entity.Cliente;
import br.edu.unipe.meuapp.model.entity.EnderecoCliente;
import br.edu.unipe.meuapp.model.entity.ItemIngredienteExtra;
import br.edu.unipe.meuapp.model.entity.ItemPedidoPizza;
import br.edu.unipe.meuapp.model.entity.Pedido;
import br.edu.unipe.meuapp.model.entity.SituacaoPedido;
import br.edu.unipe.meuapp.model.repository.PedidoRepository;
import lombok.extern.slf4j.Slf4j;

@Slf4j
@Service
public class PedidoService extends GenericCrudService<Pedido, Integer, PedidoRepository>{
	
	@Autowired
	private ClienteService clienteService;
	
	@Autowired
	private BairroEntregaService bairroEntregaService;
	
	@Autowired
	private SituacaoPedidoService situacaoPedidoService;

	@Transactional(value=TxType.REQUIRED)
	@Override
	public Pedido salvar(Pedido pedido) throws AplicacaoException {
		
		if(pedido.getId() == null) {
			List<String> detalhesValidacoes = new LinkedList<>();
			validarDadosCliente(pedido, detalhesValidacoes);
			validarDadosFormaPagamento(pedido, detalhesValidacoes);
			validarDadosFuncionario(pedido, detalhesValidacoes);
			validarItensPedido(pedido, detalhesValidacoes);
			validarEnderecoEntrega(pedido, detalhesValidacoes);
			validarBairroEntrega(pedido, detalhesValidacoes);
			if(!detalhesValidacoes.isEmpty()) {
				throw new AplicacaoException(detalhesValidacoes);
			}
	
			if(pedido.getCliente().getId() == null) {
				pedido.getEnderecoEntrega().setBairroEntrega(pedido.getBairroEntrega());
				pedido.getEnderecoEntrega().setPrincipal(true);
				pedido.getCliente().addEnderecoCliente(pedido.getEnderecoEntrega());
			} else {
				Cliente cliente = clienteService.buscar(pedido.getCliente().getId());
				pedido.setCliente(cliente);
				
				if(pedido.getEnderecoEntrega() == null) {
					EnderecoCliente enderecoPrincipal = cliente.getEnderecos().stream().filter(endereco -> endereco.getPrincipal().booleanValue()).collect(Collectors.toList()).get(0);
					pedido.setEnderecoEntrega(enderecoPrincipal);
				} else if(pedido.getEnderecoEntrega().getId() == null) {
					pedido.getEnderecoEntrega().setCliente(cliente);
					cliente.addEnderecoCliente(pedido.getEnderecoEntrega());
				}
			}
			
			pedido.setDataHoraEmissao(LocalDateTime.now());
			
			BairroEntrega bairroEntrega = bairroEntregaService.buscar(pedido.getBairroEntrega().getId());
			pedido.setDataPrevisaoEntrega(pedido.getDataHoraEmissao().plusMinutes(bairroEntrega.getTempoEntrega()));
			
			pedido.setSituacao(situacaoPedidoService.buscar(SituacaoPedido.ID_ABERTO));
			
			Pedido pedidoCadastrado = repository.saveAndFlush(pedido);
			return repository.findById(pedidoCadastrado.getId()).get();
		}
		return super.salvar(pedido);
	}
	
	@Transactional(value=TxType.REQUIRED)
	public Pedido entregar(Integer idPedido, OperacaoPedido operacaoPedido) throws AplicacaoException {
		Pedido pedido = buscar(idPedido);
		pedido.setDataHoraEntrega(operacaoPedido.getMomento());
		pedido.setMotivoDevolucao(null);
		pedido.setSituacao(situacaoPedidoService.buscar(SituacaoPedido.ID_ENTREGUE));
		Pedido pedidoCadastrado = repository.saveAndFlush(pedido);
		return repository.findById(pedidoCadastrado.getId()).get();
	}
	
	@Transactional(value=TxType.REQUIRED)
	public Pedido devolver(Integer idPedido, OperacaoPedido operacaoPedido) throws AplicacaoException {
		Pedido pedido = buscar(idPedido);
		pedido.setDataHoraEntrega(operacaoPedido.getMomento());
		pedido.setMotivoDevolucao(operacaoPedido.getMotivo());
		pedido.setSituacao(situacaoPedidoService.buscar(SituacaoPedido.ID_RETORNO_ENTREGA));
		Pedido pedidoCadastrado = repository.saveAndFlush(pedido);
		return repository.findById(pedidoCadastrado.getId()).get();
	}

	private void validarEnderecoEntrega(Pedido pedido, List<String> detalhesValidacoes) {
		if(pedido.getEnderecoEntrega() == null) {
			detalhesValidacoes.add("É necessáiro informar o endereco de entrega.");
		}
	}
	
	private void validarBairroEntrega(Pedido pedido, List<String> detalhesValidacoes) {
		if(pedido.getBairroEntrega() == null 
				|| pedido.getBairroEntrega().getId() == null) {
			detalhesValidacoes.add("É necessáiro informar o bairro de entrega.");
		}
	}

	private void validarItensPedido(Pedido pedido, List<String> detalhesValidacoes) {
		if(CollectionUtils.isEmpty(pedido.getItensPizzas())) {
			detalhesValidacoes.add("É necessáiro informar pelo menos um item para o pedido.");
		} else {
			int indiceItemPizza = 0;
			for (ItemPedidoPizza itemPizza : pedido.getItensPizzas()) {
				itemPizza.setPedido(pedido);
				if(itemPizza.getId() != null  || itemPizza.getPizza() == null || itemPizza.getPizza().getId() == null 
						|| itemPizza.getQuantidade() == null  || itemPizza.getQuantidade() == 0 
						|| itemPizza.getTamanho() == null) {
					detalhesValidacoes.add(String.format("O item de índice %d do pedido precisa ter pizza, quantidade e tamanho (que devem ser P, M ou G).", indiceItemPizza));
				}
				validarIngredientesExtrasItem(detalhesValidacoes, indiceItemPizza, itemPizza);
				indiceItemPizza++;
			}
		}
	}

	private void validarIngredientesExtrasItem(List<String> detalhesValidacoes, int indiceItemPizza, ItemPedidoPizza itemPizza) {
		if(!CollectionUtils.isEmpty(itemPizza.getIngredientesExtras())) {
			int indiceIngrediente = 0;
			for (ItemIngredienteExtra itemIngrediente : itemPizza.getIngredientesExtras()) {
				itemIngrediente.setItemPedidoPizza(itemPizza);
				if(itemIngrediente.getIngredienteExtra() == null || itemIngrediente.getIngredienteExtra().getId() == null 
						|| itemIngrediente.getQuantidade() == null || itemIngrediente.getQuantidade() == 0) {
					detalhesValidacoes.add(String.format("O o ingrediente extra de índice %d do  item de índice %d do pedido precisa ter o ingrediente e a quantidade.", indiceIngrediente, indiceItemPizza));
				}
				indiceIngrediente++;
			}
		}
	}

	private void validarDadosFuncionario(Pedido pedido, List<String> detalhesValidacoes) {
		if(pedido.getFuncionario() == null || pedido.getFuncionario().getId() == null) {
			detalhesValidacoes.add("É necessáiro informar um funcionário (apenas o id).");
		}
	}

	private void validarDadosFormaPagamento(Pedido pedido, List<String> detalhesValidacoes) {
		if(pedido.getFormaPagamento() == null || pedido.getFormaPagamento().getId() == null) {
			detalhesValidacoes.add("É necessáiro informar a forma de pagamento (apenas o id).");
		}
	}

	private void validarDadosCliente(Pedido pedido, List<String> detalhesValidacoes) {
		if(pedido.getCliente() == null) {
			detalhesValidacoes.add("É necessário informar um cliente com seu id ou um cliente com seus demais dados preenchidos para o cadastro junto com o pedido.");
		} else if (pedido.getCliente().getId() == null 
				&& (StringUtils.isBlank(pedido.getCliente().getNome()) || StringUtils.isBlank(pedido.getCliente().getTelefone1()))) {
			detalhesValidacoes.add("Se não for informado um cliente já cadastrado (valor para id) então é necessário informar o nome, o telefone1 e o pelo menos um endereço.");
		} 
	}
	
	@Scheduled(fixedDelay=60000)
	public void asyncMethodWithReturnType() {
	    log.info("Execute method asynchronously - "
	      + Thread.currentThread().getName());
    	
	    LocalDateTime inicioDia = LocalDateTime.now().withHour(0).withMinute(0).withSecond(0).withNano(0);
	    List<Pedido> pedidos = repository.buscarPedidosSituacaoPeriodo(SituacaoPedido.ID_ENTREGUE, inicioDia, LocalDateTime.now());
    	
	    log.info("================ Pedidos Entregues Hoje =================");
    	log.info("Pedidos Entregues Encontrados: " + pedidos.size());
    	log.info("Listando pedidos: ");
    	Float faturamentoTotal = 0.0f;
    	for (Pedido pedido : pedidos) {
    		Float totalPedido = pedido.getTotalPedido();
			log.info(String.format("Pedido[%d]: [R$ %.2f]", pedido.getId(), totalPedido));
			faturamentoTotal += totalPedido;
		}
    	log.info(String.format("FATURAMENTO TOTAL: R$ %.2f", faturamentoTotal));
    	log.info("=========================================================");
	}
	
}
