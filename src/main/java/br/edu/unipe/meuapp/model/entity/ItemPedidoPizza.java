package br.edu.unipe.meuapp.model.entity;

import java.util.List;import java.util.stream.Collector;
import java.util.stream.Collectors;

import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.persistence.Transient;
import javax.validation.constraints.Min;
import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;

import org.springframework.util.CollectionUtils;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonProperty;

import lombok.Data;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@NoArgsConstructor
@Data
@Getter
@Setter
@Entity
@Table(name="itens_pizzas")
public class ItemPedidoPizza extends GenericEntity<Integer>{
	
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Integer id;
	
	@NotNull(message="Quantidade é obrigatória")
	@Min(value=1)
	private Integer quantidade;
	
	@NotNull(message="Tamanho é obrigatório")
	@Enumerated(EnumType.STRING)
	private TamanhoPizza tamanho;
	
	@JsonIgnore
	@ManyToOne
	@JoinColumn(name="pedido_id")
	private Pedido pedido;
	
	@ManyToOne
	@JoinColumn(name="pizza_id")
	private Pizza pizza;
	
	private String observacao;
	
	@OneToMany(mappedBy="itemPedidoPizza", cascade = CascadeType.ALL, fetch = FetchType.EAGER)
	private List<ItemIngredienteExtra> ingredientesExtras;
	
	
	@JsonProperty(value="subTotalItem")
	@Transient
	public Float getSubTotalItem() {
		return quantidade * pizza.getPrecoTamanho(tamanho);
	}
	
	@JsonProperty(value="subTotalIngredientesExtras")
	@Transient
	public Float getSubTotalngredientesExtras() {
		if(!CollectionUtils.isEmpty(ingredientesExtras)) {
			return ingredientesExtras.stream()
					.map(ingrediente -> ingrediente.getQuantidade() * ingrediente.getIngredienteExtra().getPrecoTamanho(tamanho))
					.collect(Collectors.toList()).stream()
					.reduce(0.0f, (a, b) -> a + b);
		}
		return 0.0f;
	}
	
	@JsonProperty(value="totalItem")
	@Transient
	public Float getTotalItem() {
		return getSubTotalItem() + getSubTotalngredientesExtras();
	}
	
}
