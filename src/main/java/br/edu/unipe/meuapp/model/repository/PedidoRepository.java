package br.edu.unipe.meuapp.model.repository;

import java.time.LocalDateTime;
import java.util.List;

import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import br.edu.unipe.meuapp.model.entity.Pedido;

public interface PedidoRepository extends GenericCrudRepository<Pedido, Integer> {

	
	@Query(value="select p.* from pedidos p where p.situacao_pedido_id = :idSituacao and p.data_hora_entrega between :dataInicio and :dataFim", nativeQuery=true)
	public List<Pedido> buscarPedidosSituacaoPeriodo(@Param("idSituacao") Integer idSituacao, @Param("dataInicio") LocalDateTime dataInicio, @Param("dataFim") LocalDateTime dataFim);
//	
//	@Query("select c from Contato c where c.nome like :nome")
//	public List<Contato> buscarPorNome(@Param("nome") String nome);
//	
//	public List<Contato> findByNomeContaining(String nome);
//	
//	@Modifying
//	@Query("update Contato c set c.nome=:nome where c.email=:email")
//	public void atualizaNomePeloEmail(@Param("nome") String nome, @Param("email") String email);
}
