package br.edu.unipe.meuapp.model.repository;

import br.edu.unipe.meuapp.model.entity.Cliente;

public interface ClienteRepository extends GenericCrudRepository<Cliente, Integer> {

	
//	@Query(value="select c.nome from pedidos c where c.id = :id", nativeQuery=true)
//	public String buscarPorNomeSQLNativo(@Param("id") Long id);
//	
//	@Query("select c from Contato c where c.nome like :nome")
//	public List<Contato> buscarPorNome(@Param("nome") String nome);
//	
//	public List<Contato> findByNomeContaining(String nome);
//	
//	@Modifying
//	@Query("update Contato c set c.nome=:nome where c.email=:email")
//	public void atualizaNomePeloEmail(@Param("nome") String nome, @Param("email") String email);
}
