package br.edu.unipe.meuapp.model.entity;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.validation.constraints.NotEmpty;

import lombok.Data;
import lombok.NoArgsConstructor;

@NoArgsConstructor
@Data
//@Entity
//@Table(name="contatos")
public class Contato extends GenericEntity<Long> {
	
	private Long id;
	
	@NotEmpty(message="Nome é obrigatório")
	private String nome;
	
	@NotEmpty(message="Email é obrigatório")
	private String email;
	
	private Boolean ativo;
	
	
}
