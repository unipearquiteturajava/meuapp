package br.edu.unipe.meuapp.batch;

import javax.sql.DataSource;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.batch.core.Job;
import org.springframework.batch.core.Step;
import org.springframework.batch.core.configuration.annotation.EnableBatchProcessing;
import org.springframework.batch.core.configuration.annotation.JobBuilderFactory;
import org.springframework.batch.core.configuration.annotation.StepBuilderFactory;
import org.springframework.batch.core.launch.support.RunIdIncrementer;
import org.springframework.batch.item.ItemProcessor;
import org.springframework.batch.item.database.BeanPropertyItemSqlParameterSourceProvider;
import org.springframework.batch.item.database.JdbcBatchItemWriter;
import org.springframework.batch.item.file.FlatFileItemReader;
import org.springframework.batch.item.file.mapping.BeanWrapperFieldSetMapper;
import org.springframework.batch.item.file.mapping.DefaultLineMapper;
import org.springframework.batch.item.file.transform.DelimitedLineTokenizer;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.core.io.ClassPathResource;

import br.edu.unipe.meuapp.model.dto.Person;

//@Configuration
//@EnableBatchProcessing
public class PersonBatchConfiguration {
	
    private static final Logger log = LoggerFactory.getLogger(PersonBatchConfiguration.class);

    @Autowired
    public JobBuilderFactory jobBuilderFactory;

    @Autowired
    public StepBuilderFactory stepBuilderFactory;

  
    @Bean
    public Job importUserJob(JobCompletionNotificationListener listener, Step step1) {
        return jobBuilderFactory.get("importUserJob")
            .incrementer(new RunIdIncrementer())
            .listener(listener)
            .flow(step1)
            .end()
            .build();
    }

    @Bean
    public Step step1(JdbcBatchItemWriter<Person> writer) {
        return stepBuilderFactory.get("step1")
            .<Person, Person> chunk(10)
            .reader(reader())
            .processor(processor())
            .writer(writer)
            .build();
    }
    
    
    
    @Bean
    public FlatFileItemReader<Person> reader() {
    
    	FlatFileItemReader<Person> itemReader = new FlatFileItemReader<Person>();
    	itemReader.setName("personItemReader");
    	itemReader.setResource(new ClassPathResource("sample-data.csv"));
    	//.delimited()
    	
        final DefaultLineMapper<Person> lineMapper = new DefaultLineMapper<Person>();
        final DelimitedLineTokenizer lineTokenizer = new DelimitedLineTokenizer();
        lineTokenizer.setNames(new String[]{"firstName", "lastName"});
        lineMapper.setLineTokenizer(lineTokenizer);
        
        final BeanWrapperFieldSetMapper<Person> fieldMapper = new BeanWrapperFieldSetMapper<>();
        fieldMapper.setTargetType(Person.class);
        lineMapper.setFieldSetMapper(fieldMapper);
        itemReader.setLineMapper(lineMapper);
        
        return itemReader;
    }

    @Bean
    public PersonItemProcessor processor() {
        return new PersonItemProcessor();
    }

    @Bean
    public JdbcBatchItemWriter<Person> writer(DataSource dataSource) {
    	
    	JdbcBatchItemWriter<Person> itemWriter = new JdbcBatchItemWriter<Person>();
    	itemWriter.setItemSqlParameterSourceProvider(new BeanPropertyItemSqlParameterSourceProvider<>());
    	itemWriter.setSql("INSERT INTO people (first_name, last_name) VALUES (:firstName, :lastName)");
    	itemWriter.setDataSource(dataSource);
    	
    	return itemWriter;
    }

    
    public class PersonItemProcessor implements ItemProcessor<Person, Person> {



        @Override
        public Person process(final Person person) throws Exception {
            final String firstName = person.getFirstName().toUpperCase();
            final String lastName = person.getLastName().toUpperCase();

            final Person transformedPerson = new Person(firstName, lastName);

            log.info("Converting (" + person + ") into (" + transformedPerson + ")");

            return transformedPerson;
        }

    }
}
