package br.com.cliente.cas.service;

import java.io.IOException;
import java.net.URLEncoder;
import java.nio.charset.StandardCharsets;
import java.util.Arrays;
import java.util.Base64;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.beanutils.BeanToPropertyValueTransformer;
import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.lang3.StringUtils;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.authentication.InsufficientAuthenticationException;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;
import org.springframework.web.client.HttpClientErrorException;
import org.springframework.web.client.RestTemplate;

import com.fasterxml.jackson.annotation.JsonAutoDetect;
import com.fasterxml.jackson.core.JsonParseException;
import com.fasterxml.jackson.databind.DeserializationFeature;
import com.fasterxml.jackson.databind.JsonMappingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.introspect.VisibilityChecker;
import com.indracompany.core.exception.CustomException;
import com.indracompany.core.modelo.to.ExceptionVO;
import com.indracompany.core.util.FormatadorIndraCore;
import com.indracompany.core.util.WSClientUtil;
import com.indracompany.core.util.WebUtils;
import com.indracompany.core.util.generico.InstanceUtil;
import com.indracompany.core.util.generico.MessagesProperty;
import com.indracompany.core.util.json.JSONUtil;

import br.com.cliente.cas.dto.UsuarioDTO;
import br.com.cliente.cas.dto.UsuarioPerfilDTO;


@Service(value="userService")
public class UserServiceImpl implements UserDetailsService {

    private org.slf4j.Logger logger = LoggerFactory.getLogger(UserServiceImpl.class);

    @Autowired
    private HttpServletRequest request;

    @Autowired
    private HttpServletResponse response;

	@Override
	public UserDetails loadUserByUsername(String noLogin) {

		try {

			String usuariobase64 = new String( Base64.getDecoder().decode( noLogin ), "UTF-8" );
			UsuarioDTO usuario =
					JSONUtil.convertJsonStringToObject(usuariobase64, UsuarioDTO.class);

			if ( usuario == null || StringUtils.isEmpty( usuario.getNoLogin() ) ) {

				throw new UsernameNotFoundException("Acesso Negado - Não foi possível carregar os dados do usuário. ");
			}

			UsuarioPerfilDTO usuarioPerfil = new UsuarioPerfilDTO();
			usuarioPerfil.setSqUsuario(usuario.getSqUsuario());


			UsuarioPerfilDTO[] usuarioPerfis = carregarPerfis(usuario);

		
			List<GrantedAuthority> authorities = Arrays.asList(usuarioPerfis);

			usuario.setAuthorities(authorities);
			return usuario;

		} catch (Exception ex) {



            throw new InsufficientAuthenticationException(ex.getLocalizedMessage());

		}
	}



	private UsuarioPerfilDTO[] carregarPerfis(UsuarioDTO usuario) throws CustomException {
	    UsuarioPerfilDTO[] usuarioPerfis =  get(usuario);
        usuario.setPerfil(Arrays.asList(usuarioPerfis));
		return usuarioPerfis;
	}


}
