package br.com.cliente.cas.authentication.handler;

import java.io.IOException;
import java.security.GeneralSecurityException;
import java.security.cert.X509Certificate;
import java.util.Base64;
import java.util.Map;

import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.lang.StringUtils;
import org.jasig.cas.adaptors.x509.authentication.principal.X509CertificateCredential;
import org.jasig.cas.adaptors.x509.util.CertUtils;
import org.jasig.cas.authentication.Credential;
import org.jasig.cas.authentication.HandlerResult;
import org.jasig.cas.authentication.PreventedException;
import org.jasig.cas.authentication.handler.support.AbstractPreAndPostProcessingAuthenticationHandler;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.binding.message.MessageBuilder;
import org.springframework.binding.message.MessageContext;
import org.springframework.web.client.HttpClientErrorException;
import org.springframework.web.client.RestTemplate;
import org.springframework.webflow.execution.RequestContextHolder;

import com.fasterxml.jackson.annotation.JsonAutoDetect;
import com.fasterxml.jackson.core.JsonParseException;
import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.DeserializationFeature;
import com.fasterxml.jackson.databind.JsonMappingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.introspect.VisibilityChecker;
import br.com.cliente.cas.authentication.JSONUtil;

import br.com.cliente.cas.authentication.x509.UsuarioDTO;
import br.com.cliente.cas.authentication.x509.WSClientUtil;
import br.com.cliente.cas.authentication.x509.X509CertificadoICPBrasil;
import br.com.cliente.cas.cas.ParametrosCas;

public class X509CertificadoAuthenticationHandler extends AbstractPreAndPostProcessingAuthenticationHandler {

	private org.slf4j.Logger logger = LoggerFactory.getLogger(X509CertificadoAuthenticationHandler.class);

	public static final String COOKIE_AUTENTICACAO_X509 = "autenticacaoDTEX509";

	@Autowired
	private HttpServletRequest request;

	@Autowired
	private HttpServletResponse response;



	public boolean supports(Credential credential) {
		Boolean result = Boolean.valueOf(
				(credential != null) && (X509CertificateCredential.class.isAssignableFrom(credential.getClass())));
		return result.booleanValue();
	}

	protected HandlerResult doAuthentication(Credential credential) throws GeneralSecurityException, 
			PreventedException {
		
		X509CertificateCredential x509Credential = (X509CertificateCredential) credential;
		X509Certificate[] certificates = x509Credential.getCertificates();
		X509Certificate clientCert = null;
		MessageContext messageContext =
			    RequestContextHolder.getRequestContext().getMessageContext();
		final MessageBuilder builder = new MessageBuilder();

		try {

			if (certificates!= null) {
				
				for (X509Certificate certificate : certificates) {
					
					this.logger.debug("Evaluating {}", CertUtils.toString(certificate));

					int pathLength = certificate.getBasicConstraints();
					if (pathLength < 0) {
						this.logger.debug("Found valid client certificate");
						clientCert = certificate;
					} else {
						this.logger.debug("Found valid CA certificate");
					}
				}
				
				java.util.logging.Logger.getLogger("Capturando cpf...");
				
				if (clientCert != null) {

					X509CertificadoICPBrasil certICPBrasil = new X509CertificadoICPBrasil(clientCert);

					if ( StringUtils.isNotEmpty( certICPBrasil.getCNPJ() )
							&& StringUtils.isEmpty( certICPBrasil.getCPF() ) ) {
						
						messageContext.addMessage(builder.error().code("authenticationFailure.CertificadoInvalidoContribuinteException").build());
						throw new GeneralSecurityException();
					}

					if ( StringUtils.isEmpty( certICPBrasil.getCPF() ) ) {
						messageContext.addMessage(builder.error().code("authenticationFailure.CertificadoInvalidoContribuinteException").build());
						throw new GeneralSecurityException();
					}

					java.util.logging.Logger.getLogger(certICPBrasil.getCPF());
					if ((certICPBrasil.getCPF() != null) && (!certICPBrasil.getCPF().isEmpty())) {

						String certificadoBase64 = 
								Base64.getEncoder().encodeToString( clientCert.getEncoded() );

						UsuarioDTO usuario = logarUsuarioCertificado(certICPBrasil.getCPF());
						
						String usuariobase64 = Base64.getEncoder().encodeToString(
								JSONUtil.convertObjectToJsonString(usuario).getBytes("UTF8") );

						return createHandlerResult(credential,
								this.principalFactory.createPrincipal(usuariobase64), null);
					}
				}
			}

		} catch (Exception ex) {

			logger.error("############################", ex);
			logger.error("ERROR DURANTE A VALIDAÇÃO DO USUARIO");
			logger.error("############################");

			messageContext.addMessage(builder.error().defaultText(ex.getLocalizedMessage()).build());
		}

		throw new GeneralSecurityException();
	}


	private UsuarioDTO logarUsuarioCertificado(RestTemplate rest, String cpf) throws Exception {

	
		UsuarioDTO usuarioEnvio = new UsuarioDTO();

		usuarioEnvio.setDsSenha(password);

		usuarioEnvio.setNoLogin(username);

		return usuarioRetorno;
		
	}



}
