package br.com.cliente.cas.authentication.handler;

import java.io.IOException;
import java.security.GeneralSecurityException;
import java.text.SimpleDateFormat;
import java.util.Base64;
import java.util.Date;
import java.util.Locale;
import java.util.Map;

import javax.security.auth.login.FailedLoginException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.jasig.cas.authentication.HandlerResult;
import org.jasig.cas.authentication.PreventedException;
import org.jasig.cas.authentication.UsernamePasswordCredential;
import org.jasig.cas.authentication.handler.support.AbstractUsernamePasswordAuthenticationHandler;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.client.HttpClientErrorException;
import org.springframework.web.client.RestTemplate;

import com.fasterxml.jackson.annotation.JsonAutoDetect;
import com.fasterxml.jackson.core.JsonParseException;
import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.DeserializationFeature;
import com.fasterxml.jackson.databind.JsonMappingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.introspect.VisibilityChecker;
import br.com.cliente.cas.authentication.JSONUtil;

import br.com.cliente.cas.authentication.x509.UsuarioDTO;
import br.com.cliente.cas.authentication.x509.WSClientUtil;


public class UsernamePasswordAuthenticationHandler extends AbstractUsernamePasswordAuthenticationHandler {
	private final Logger log = LoggerFactory.getLogger(UsernamePasswordAuthenticationHandler.class);

	@Autowired
	private HttpServletRequest request;

	@Autowired
	private HttpServletResponse response;
	


	protected final HandlerResult authenticateUsernamePasswordInternal(UsernamePasswordCredential credential)
			throws GeneralSecurityException, PreventedException {

		String username = credential.getUsername();

		String password = credential.getPassword();

		try {
					

			UsuarioDTO usuario = verificarUsuarioSenha(username, password);
			
			String usuariobase64 = Base64.getEncoder().encodeToString(
					JSONUtil.convertObjectToJsonString(usuario).getBytes("UTF8") );

			return createHandlerResult(credential, this.principalFactory.createPrincipal(usuariobase64), null);
			
			
		} catch (Exception ex) {

			logger.error("############################");
			logger.error("ERROR DURANTE A VALIDAÇÃO DO USUARIO");
			logger.error("############################");

			throw new FailedLoginException(ex.getLocalizedMessage());
		}
	}

	private UsuarioDTO verificarUsuarioSenha(String username, String password) throws Exception {


		UsuarioDTO usuarioEnvio = new UsuarioDTO();

		usuarioEnvio.setDsSenha(password);

		usuarioEnvio.setNoLogin(username);

		return usuarioRetorno;
	}

	
}
